import mysql from 'mysql2';
import http from 'http';
import Sequelize from 'sequelize';

const sequelize = new Sequelize('pets', 'root', 'mazda8245', {
    host: 'localhost',
    dialect: 'mysql'
});

const hostname = '127.0.0.1';
const port = 4000;

const Dogs = sequelize.define('dogs', {
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    owner: {
        type: Sequelize.STRING,
        allowNull: false
    },
    birth: {
        type: Sequelize.DATE,
        allowNull: false
    }
});

await sequelize.sync();

await Dogs.create({
    name: 'Sandy',
    owner: 'Lennon',
    birth: '2015-01-03'
});

await Dogs.create({
    name: 'Cookie',
    owner: 'Casey',
    birth: '2013-11-13'
});

await Dogs.create({
    name: 'Charlie',
    owner: 'River',
    birth: '2016-05-21'
});

await Dogs.create({
    name: 'San',
    owner: 'Len',
    birth: '2010-01-03'
});

await Dogs.create({
    name: 'Char',
    owner: 'River',
    birth: '2019-06-01'
});

const server = http.createServer(async (req, res) => {
    res.statusCode = 200;

    const dogs = await Dogs.findAll({
        where: { owner: 'River' },
        attributes: ['id', 'name', 'owner', 'birth']
    });

    res.setHeader('Content-Type', 'text/plain');
    res.end(JSON.stringify(dogs));
});

server.listen(port, hostname, () => {
    console.log('server running');
});
