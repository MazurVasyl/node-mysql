import mysql from 'mysql2';
import http from 'http';

const hostname = '127.0.0.1';
const port = 3000;

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'mazda8245',
    database: 'pets'
});

try {
    connection.promise()
        .execute('CREATE TABLE dogs ' +
            '(id INT unsigned NOT NULL AUTO_INCREMENT, name VARCHAR(150) NOT NULL, owner VARCHAR(150) NOT NULL, birth DATE NOT NULL, PRIMARY KEY (id))'
        );

    connection.promise()
        .execute('INSERT INTO dogs ( name, owner, birth) VALUES ' +
            '( \'Sandy\', \'Lennon\', \'2015-01-03\' ), ' +
            '( \'Cookie\', \'Casey\', \'2013-11-13\' ), ' +
            '( \'Charlie\', \'River\', \'2016-05-21\' ), ' +
            '( \'San\', \'Len\', \'2010-01-03\' ), ' +
            '( \'Char\', \'River\', \'2019-06-01\' )'
        );
} catch (error) {
    console.log(error);
}

const server = http.createServer(async (req, res) => {
    const [rows] = await connection.promise()
        .execute('SELECT * FROM dogs WHERE owner = \'River\'');
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');

    res.end(JSON.stringify(rows));
});

server.listen(port, hostname, () => {
    console.log('server running');
});
